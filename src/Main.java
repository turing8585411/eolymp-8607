import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        double a =(x/1000)*(x/1000)+(x/100%10)*(x/100%10)+(x/10%10)*(x/10%10)+(x%10)*(x%10);

        System.out.printf("%.0f",a);

    }
}